
/**
 * Clasa abstracta care extinde subclasele IntMonomial si ReMonomial
 * Sunt definite clasele care urmeaza sa fie implementate in subclasele IntMonomial si ReMonomial
 * @author Pode
 * degree ->gradul Monomului (int)
 * coefficient -> coeficientul Monomului (Number)
 * 
 */

package entities;

 public abstract class Monomial {
	protected int degree;
	protected Number coefficient;
	
	
	public abstract void setDegree(int degree);
	
	public abstract int getDegree();
	
	public abstract void setCoefficient(Number coefficient);
	
	public abstract Number getCoefficient();
	
	public abstract Monomial addition(Monomial monomial);
	
	public abstract Monomial substraction(Monomial monomial);
	
	public abstract Monomial multiplyWithMonomial(Monomial monomial);
	
	public abstract String toString();
}
