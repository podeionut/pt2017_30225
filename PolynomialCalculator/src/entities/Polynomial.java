package entities;

import java.util.*;
import java.util.stream.Collectors;

public class Polynomial {
	private List<Monomial> list;
	
	public Polynomial(){
		list = new ArrayList<Monomial>();
	}
	
	//Adauga un monom in lista
	public void addElement(Monomial monomial){
		list.add(monomial);
	}
	
	//Sterge un monom din lista
	public void removeElement(Monomial monomial){
		list.remove(monomial);
	}
	
	
	//Verifica daca sunt monoame cu coeficientul 0 si le sterge
	public Polynomial removeElementsWithZeroCoeff(){
		Polynomial pol = new Polynomial();
		pol.list = this.list;
		Iterator<Monomial> i = this.list.iterator();
		while(i.hasNext())
		{
			Monomial mon = (Monomial)i.next();
			if(mon.getCoefficient() == (Number)0) pol.removeElement(pol.list.get(0));
		}
		return pol;
	}
	
	//Cautarea unui monom cu un anumit grad.
	public Monomial searchDegree(int degree)
	{
		Iterator<Monomial> i = this.list.iterator();
		while(i.hasNext())
		{
			Monomial monom = (Monomial) i.next();
			if(monom.getDegree() == degree)
				return monom;
		}
		return null;
	}
	/**
	 * Suma a doua polinoame
	 * 
	 * @param pol
	 * @return
	 */
	public Polynomial addition(Polynomial pol){
		Polynomial result = new Polynomial();
		result.list = this.list;
		
		for(Monomial current : pol.list)
		{
			Monomial monom = new IntMonomial(0,0);
			if(searchDegree(current.getDegree()) != null)
			{
				monom = searchDegree(current.getDegree()).addition(current);
				removeElement(searchDegree(current.getDegree()));
				addElement(monom);
				
			}
			else result.addElement(current);
		}
		return result;
	}
	//Scaderea a doua polinoame
	
	
	public Polynomial substraction(Polynomial pol){
		Polynomial result = new Polynomial();
		result.list = this.list;
		
		for(Monomial current : pol.list)
		{
			Monomial monom = new IntMonomial(0,0);
			if(searchDegree(current.getDegree()) != null)
			{
				monom = searchDegree(current.getDegree()).substraction(current);
				removeElement(searchDegree(current.getDegree()));
				addElement(monom);
				
			}
			else result.addElement(current);
		}
		return result;
	}
	
	
	//Inmultirea a doua polinoame
	
	public Polynomial multiplicationWithPolynomial(Polynomial pol){
		Polynomial result = new Polynomial();
		Polynomial auxiliary = new Polynomial();
		Iterator<Monomial> i = this.list.iterator();
		while(i.hasNext())
		{
			result.addElement(new IntMonomial(0,0));
			Monomial mon = i.next();
			Iterator<Monomial> j = pol.list.iterator();
			while(j.hasNext())
			{
				Monomial mon2 = j.next();
				auxiliary.addElement(mon.multiplyWithMonomial(mon2));
			}
		}
		result = result.addition(auxiliary);
		return result;
	}
	

	//Integrarea unui polinom

	@Override
	public String toString()
	{
		String a = new String();
		String finalStr = new String();
		Iterator<Monomial> i = list.iterator();
		while(i.hasNext()){
			Monomial currentMon = i.next();
			a = currentMon.toString();
			if(finalStr == " ")
				finalStr =  a;
			else finalStr = finalStr + " " + a;
		}
		return finalStr;
	}
	
	public List<Monomial> getList(){
		
		return this.list;
	}
	
}
