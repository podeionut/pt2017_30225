package entities;

public class IntMonomial extends Monomial{
	
	private Integer coefficient;
	private int degree;
	
	// Clasa Constructor
	public IntMonomial(Integer coefficient, int degree){
		setDegree(degree);
		setCoefficient(coefficient);
	}
	
	@Override
	public void setDegree(int degree) {
		this.degree = degree;
	}

	@Override
	public int getDegree() {
		return degree;
	}

	@Override
	public void setCoefficient(Number coefficient) {
		this.coefficient = (Integer) coefficient;
	}

	
	public Number getCoefficient() {
		return (Integer)coefficient;
	}

	/**
	 * Adunarea a doua monoame
	 *  Daca gradul lor este egal, coeficientii sunt adunati iar  @result este returnat
	 */

	public Monomial addition(Monomial monomial) {
		if(monomial.getDegree() == this.degree)
		{
			Monomial result = new IntMonomial(0,0);
			Integer coeff = this.coefficient + (Integer)monomial.getCoefficient();
			result.setCoefficient(coeff);
			result.setDegree(this.degree);
			
			return result;
		}
		return null;
	}

	/**
	 *  Scaderea a doua monoame
	 *  
	 */
	@Override
	public Monomial substraction(Monomial monomial) {
		if(monomial.getDegree() == this.degree)
		{
			Monomial result = new IntMonomial(0,0);
			Integer coeff = this.coefficient - (Integer)monomial.getCoefficient();
			result.setCoefficient(coeff);
			result.setDegree(this.degree);
			
			return result;
		}
		return null;
	}

	@Override
	public Monomial multiplyWithMonomial(Monomial monomial) {
		Monomial result = new IntMonomial(0,0);
		result.setCoefficient(this.coefficient * (Integer)monomial.getCoefficient());
		result.setDegree(this.degree + monomial.getDegree());
		return result;
	}


	@Override
	public String toString() {
		if(this.coefficient != 0)
			if(this.coefficient < 0)
				return this.coefficient + "x^" + this.degree;
			else
				return "+" + this.coefficient + "x^" + this.degree;
		return "";
	}

}
