package application;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import entities.IntMonomial;
import entities.Polynomial;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interface {

	private JFrame frame;
	private JTextField pol1;
	private JTextField pol2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface window = new Interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 532, 371);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPrimulPolinom = new JLabel("Primul Polinom");
		lblPrimulPolinom.setBounds(10, 35, 109, 27);
		frame.getContentPane().add(lblPrimulPolinom);
		
		JLabel lblAlDoileaPolinom = new JLabel("Al doilea Polinom");
		lblAlDoileaPolinom.setBounds(10, 99, 109, 27);
		frame.getContentPane().add(lblAlDoileaPolinom);
		
		pol1 = new JTextField();
		pol1.setBounds(183, 23, 245, 42);
		frame.getContentPane().add(pol1);
		pol1.setColumns(10);
		
		pol2 = new JTextField();
		pol2.setBounds(183, 102, 245, 42);
		frame.getContentPane().add(pol2);
		pol2.setColumns(10);
		
		JLabel lblRez = new JLabel("Rezultatul va fi afisat aici");
		lblRez.setBounds(183, 155, 245, 23);
		frame.getContentPane().add(lblRez);
		
		JButton Adn = new JButton("Adunare");
		Adn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str1 = pol1.getText();
				Polynomial pol1 = new Polynomial();
				pol1 = createPolynomialFromString(str1);
				String str2 = pol2.getText();
				Polynomial pol2 = new Polynomial();
				pol2 = createPolynomialFromString(str2);
				if(pol1 != null && pol2 != null) {
				lblRez.setText((pol1.addition(pol2).toString()));
				}
					
				
			}
		});
		
		Adn.setBounds(10, 200, 118, 27);
		frame.getContentPane().add(Adn);
		
		JButton Scadere = new JButton("Scadere");
		Scadere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str1 = pol1.getText();
				Polynomial pol1 = new Polynomial();
				pol1 = createPolynomialFromString(str1);
				String str2 = pol2.getText();
				Polynomial pol2 = new Polynomial();
				pol2 = createPolynomialFromString(str2);
				if(pol1 != null && pol2 != null) 
					lblRez.setText((pol1.substraction(pol2)).toString());
			}
		});
		Scadere.setBounds(156, 200, 118, 27);
		frame.getContentPane().add(Scadere);
		
		JLabel lblRezultat = new JLabel("Rezultat");
		lblRezultat.setBounds(10, 157, 109, 32);
		frame.getContentPane().add(lblRezultat);
		
		JButton btnInmultire = new JButton("Inmultire");
		btnInmultire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str1 = pol1.getText();
				Polynomial pol1 = new Polynomial();
				pol1 = createPolynomialFromString(str1);
				String str2 = pol2.getText();
				Polynomial pol2 = new Polynomial();
				pol2 = createPolynomialFromString(str2);
				if(pol1 != null && pol2 != null) 
					lblRez.setText((pol1.multiplicationWithPolynomial(pol2)).toString());
			}
		});
		btnInmultire.setBounds(307, 200, 126, 27);
		frame.getContentPane().add(btnInmultire);
		
		
	}
	private Polynomial createPolynomialFromString(String str)
	{
	if( validation(str) == false)
		{
			errorPopUp();
			return null;
		}
		Polynomial pol = new Polynomial();
		int i = str.length() - 1;
		int degree = 0;
		while(i >= 0)
		{
			if(str.charAt(i) == ' ')
			{
				degree++;
			}
			i--;
		}
		
		i = 0;
		while(i < str.length())
		{
			
				if(str.charAt(i) == '-' || Character.isDigit(str.charAt(i)))
				{
					String aux = new String(); 
					while(str.charAt(i) != ' ') //gasire  coeficient
					{
						aux = aux + str.charAt(i);
						i++;
						if(i == str.length()) break;
					}
					pol.addElement(new IntMonomial(Integer.parseInt(aux), degree));
					degree--;
				}
				else if(str.charAt(i) != ' ')
				{
					pol.addElement(new IntMonomial(Integer.parseInt(String.valueOf(str.charAt(i))), degree));
					degree--;
				}
			
			i++;
		}
		return pol;
	}
	
	private boolean validation(String str)
	{
		for(int i = 0; i< str.length(); i++)
		{
			if(Character.isDigit(str.charAt(i)) == true)
				return true;
		}
		return false;
	}
	
	private void errorPopUp()
	{
		JFrame frame = new JFrame("Error");
		JOptionPane.showMessageDialog(frame, "Polinomul scris nu este valid. Va rugam introduceti numai coeficientii numerici.", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	private void infoPopUp()
	{
		JFrame frame = new JFrame("Information Message");
		JOptionPane.showMessageDialog(frame, "Info: Urmatoarea operatie va fi efectuata doar pentru primul polinom!");
	}

}
